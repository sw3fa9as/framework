<?php

class Database {

	private static $connection;

	private function __construct() {}

	public static function connect($hostname, $database, $username, $password) {
		if ( ! self::$connection) {
			try {
				self::$connection = new PDO('mysql:dbname=' . $database . ';host=' . $hostname, $username, $password);
				self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
				self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $error) {
				return show_500();
			}
		}

		return self::$connection;
	}
}

# End of file