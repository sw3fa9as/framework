<?php

class Base_Model extends Core {

	public function __construct() {
		parent::__construct();

		if ($this->config->item('autoload')) {
			$this->database_connect();
		}

		$this->table = $this->get_table();
	}

	protected function database_connect() {
		$hostname = $this->config->item('hostname');
		$database = $this->config->item('database');
		$username = $this->config->item('username');
		$password = $this->config->item('password');

		$this->database = Database::connect($hostname, $database, $username, $password);
	}

	private function get_table() {
		$table = get_class($this);
		$table = str_replace('_Model', '', $table);
		$table = strtolower($table);

		return $table;
	}

	public function insert() {
		$this->query['insert'] = TRUE;

		return $this;
	}

	public function into($table) {
		$this->table = $table;

		return $this;
	}

	public function values($parameters) {
		$this->query['values'] = array_values($parameters);
		$this->query['insert_columns'] = $this->get_columns($parameters);
		$this->query['insert_parameters'] = $this->get_insert_parameters($parameters);
	}

	private function get_columns($parameters) {
		$parameters = array_keys($parameters);

		return implode(", ", $parameters);
	}

	private function get_insert_parameters($parameters) {
		$count = count($parameters);
		for ($i = 1; $i <= $count; $i++) {
			if (isset($query)) {
				$query .= ", ?";
			} else {
				$query = "?";
			}
		}

		return $query;
	}

	public function select($columns) {
		$this->query['select'] = TRUE;
		$this->query['select_columns'] = $columns;

		return $this;
	}

	public function from($table) {
		$this->table = $table;

		return $this;
	}

	public function where($parameters) {
		if (isset($this->query['values'])) {
			$this->query['values'] = array_merge($this->query['values'], array_values($parameters));
		} else {
			$this->query['values'] = array_values($parameters);
		}

		$this->query['select_parameters'] = $this->get_parameters($parameters, " AND");

		return $this;
	}

	private function get_parameters($parameters, $delimiter) {
		$parameters = array_keys($parameters);
		foreach ($parameters as $parameter) {
			if (isset($query)) {
				$query .= $delimiter . " " . $parameter . " = ?";
			} else {
				$query = $parameter . " = ?";
			}
		}

		return $query;
	}

	public function group_by($columns) {
		$this->query['group_by'] = $columns;

		return $this;
	}

	public function order_by($columns) {
		$this->query['order_by'] = $columns;

		return $this;
	}

	public function limit($limit) {
		$this->query['limit'] = $limit;

		return $this;
	}

	public function update($table = NULL) {
		$this->query['update'] = TRUE;

		if (isset($table)) {
			$this->table = $table;
		}

		return $this;
	}

	public function set($parameters) {
		$this->query['values'] = array_values($parameters);
		$this->query['update_parameters'] = $this->get_parameters($parameters, ",");

		return $this;
	}

	public function delete() {
		$this->query['delete'] = TRUE;

		return $this;
	}

	protected function build_query() {
		if (isset($this->query['insert'])) {
			$query = "INSERT INTO " . $this->table;
			$query .= " (" . $this->query['insert_columns'] . ")";
			$query .= " VALUES (" . $this->query['insert_parameters'] . ")";
		}

		if (isset($this->query['select'])) {
			$query = "SELECT " . $this->query['select_columns'] . " FROM " . $this->table;
		}

		if (isset($this->query['update'])) {
			$query = "UPDATE " . $this->table;
			$query .= " SET " . $this->query['update_parameters'];
		}

		if (isset($this->query['delete'])) {
			$query = "DELETE FROM " . $this->table;
		}

		if (isset($this->query['select_parameters'])) {
			$query .= " WHERE " . $this->query['select_parameters'];
		}

		if (isset($this->query['group_by'])) {
			$query .= " GROUP BY " . $this->query['group_by'];
		}

		if (isset($this->query['order_by'])) {
			$query .= " ORDER BY " . $this->query['order_by'];
		}

		if (isset($this->query['limit'])) {
			$query .= " LIMIT " . $this->query['limit'];
		}

		return $query;
	}

	public function execute($query = NULL, $rv = 0) {
		if (empty($query)) {
			$query = $this->build_query();
		}

		try {
			$sth = $this->database->prepare($query);

			if (isset($this->query['values'])) {
				foreach ($this->query['values'] as $key => $value) {
					$sth->bindValue($key + 1, $value);
				}
			}

			$result = $sth->execute();

			unset($this->query);

			switch ($rv) {
				case 0:
					return $result;
					break;
				case 1:
					return $sth->fetch();
					break;
				case 2:
					return $sth->fetchAll();
					break;
				case 3:
					return $sth->rowCount();
					break;
			}
		} catch (PDOException $error) {
			return show_500();
		}
	}

	public function fetch($query = 0) {
		return $this->execute($query, 1);
	}

	public function fetch_all($query = 0) {
		return $this->execute($query, 2);
	}
}

# End of file