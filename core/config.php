<?php

class Config {

	private $config = array();

	public function __construct() {
		$this->add('general');
	}

	public function add($file) {
		if (is_readable(APPLICATION_PATH . '/config/' . $file . '.php')) {
			require APPLICATION_PATH . '/config/' . $file . '.php';

			$this->config = array_merge($this->config, $config);

			return TRUE;
		}

		return show_500();
	}

	public function item($key) {
		if (array_key_exists($key, $this->config)) {
			return $this->config[$key];
		}

		return show_500();
	}
}

# End of file