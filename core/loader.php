<?php

class Loader {

	public function model($file) {
		if (is_readable(APPLICATION_PATH . '/models/' . $file . '.php')) {
			require APPLICATION_PATH . '/models/' . $file . '.php';

			$model = ucfirst($file) . '_Model';
			return new $model;
		}

		return show_500();
	}

	public function view($file, $data = NULL) {
		if (is_readable(APPLICATION_PATH . '/views/' . $file . '.php')) {
			if (isset($data)) {
				extract($data);
			}

			require APPLICATION_PATH . '/views/' . $file . '.php';
			return;
		}

		return show_500();
	}

	public function library($file, $parameters = NULL) {
		if (is_readable(APPLICATION_PATH . '/libraries/' . $file . '.php')) {
			require APPLICATION_PATH . '/libraries/' . $file . '.php';

			$library = ucfirst($file);

			if (isset($parameters)) {
				$class = new ReflectionClass($library);
				return $class->newInstanceArgs($parameters);
			}

			return new $library;
		}

		return show_500();
	}
}

# End of file