<?php

class Router {

	public function __construct() {
		$this->path = $this->get_path();
		$this->segments = $this->get_segments($this->path);
	}

	public function get_controller() {
		$file = md5($this->path);
		if (is_readable(APPLICATION_PATH . '/cache/' . $file) && time() - 300 < filemtime(APPLICATION_PATH . '/cache/' . $file)) {
			require APPLICATION_PATH . '/cache/' . $file;
			return;
		}

		if (file_exists(APPLICATION_PATH . '/controllers/' . $this->segments[0] . '.php')) {
			return APPLICATION_PATH . '/controllers/' . array_shift($this->segments) . '.php';
		}

		foreach ($this->segments as $segment) {
			if (isset($this->subfolder)) {
				if (is_dir(APPLICATION_PATH . '/controllers/' . $this->subfolder . '/' . $segment)) {
					$this->set_subfolder(array_shift($this->segments));
				} elseif (file_exists(APPLICATION_PATH . '/controllers/' . $this->subfolder . '/' . $segment . '.php')) {
					return APPLICATION_PATH . '/controllers/' . $this->subfolder . '/' . array_shift($this->segments) . '.php';
				}
			} elseif (is_dir(APPLICATION_PATH . '/controllers/' . $segment)) {
				$this->set_subfolder(array_shift($this->segments));
			}
		}

		if (isset($this->subfolder) && empty($this->segments) && file_exists(APPLICATION_PATH . '/controllers/' . $this->subfolder . '/welcome.php')) {
			return APPLICATION_PATH . '/controllers/' . $this->subfolder . '/welcome.php';
		}

		return FALSE;
	}

	public function get_parameters() {
		// Warning: this only works when get_controller() is called
		return empty($this->segments) ? NULL : $this->segments;
	}

	private function set_subfolder($directory) {
		if (isset($this->subfolder)) {
			$this->subfolder .= '/' . $directory;
			return;
		}

		$this->subfolder = $directory;
	}

	private function get_path() {
		if ( ! array_key_exists('PATH_INFO', $_SERVER)) {
			return 'welcome';
		}

		$path = $_SERVER['PATH_INFO'];
		$path = trim($path, '/');

		return $path;
	}

	private function get_segments($path) {
		return explode('/', $path);
	}
}

# End of file