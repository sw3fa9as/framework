<?php

class Core {

	public function __construct() {
		$this->config = new Config();
		$this->load = new Loader();
		$this->cache = new Cache();

		$this->set_error_reporting();
	}

	private function set_error_reporting() {
		error_reporting(E_ALL);
		if (ini_set('display_errors', 1)) {
			return TRUE;
		}

		return FALSE;
	}

	public function set_high_memory($limit = '1G') {
		if (ini_set('memory_limit', $limit)) {
			return TRUE;
		}

		return FALSE;
	}
}

# End of file