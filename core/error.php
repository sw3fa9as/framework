<?php

function show_404() {
	header('HTTP/1.1 404 Not Found');

	if (is_readable(APPLICATION_PATH . '/views/errors/404.php')) {
		require APPLICATION_PATH . '/views/errors/404.php';
	} else {
		echo '404 Not Found';
	}
}

function show_500() {
	ob_clean();
	header('HTTP/1.1 500 Internal Server Error');

	if (is_readable(APPLICATION_PATH . '/views/errors/500.php')) {
		require APPLICATION_PATH . '/views/errors/500.php';
	} else {
		echo '500 Internal Server Error';
	}

	exit();
}

# End of file