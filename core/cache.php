<?php

class Cache {

	public function add($filename, $data) {
		if (is_writable(APPLICATION_PATH . '/cache/')) {
			if (is_array($data)) {
				$data = json_encode($data);
			} elseif (is_object($data)) {
				$data = serialize($data);
			}

			$fh = fopen(APPLICATION_PATH . '/cache/' . $filename, 'w');
			$result = fwrite($fh, $data);
			fclose($fh);

			return $result;
		}

		return show_500();
	}

	public function get($file) {
		if (is_readable(APPLICATION_PATH . '/cache/' . $file)) {
			$fh = fopen(APPLICATION_PATH . '/cache/' . $file, 'r');
			$data = fread($fh, filesize(APPLICATION_PATH . '/cache/' . $file));
			fclose($fh);

			$extension = pathinfo($file, PATHINFO_EXTENSION);
			if ($extension === 'json') {
				$data = json_decode($data, TRUE);
			} elseif ($extension === 'ser') {
				$data = unserialize($data);
			}

			return $data;
		}

		return show_500();
	}
}

# End of file