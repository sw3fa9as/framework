<?php

class Base_Controller extends Core {

	public function __construct() {
		parent::__construct();

		$this->caching = $this->config->item('cache');
		if ($this->caching) {
			ob_start();
		}
	}

	public function __destruct() {
		if ($this->caching) {
			$path = $this->get_path();

			$filename = md5($path);
			$data = ob_get_contents();

			$this->cache->add($filename, $data);
		}
	}

	private function get_path() {
		if ( ! array_key_exists('PATH_INFO', $_SERVER)) {
			return 'welcome';
		}

		$path = $_SERVER['PATH_INFO'];
		$path = trim($path, '/');

		return $path;
	}
}

# End of file