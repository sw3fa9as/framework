<?php

class Application {

	public function __construct() {
		$this->load_core();
	}

	public function run() {
		$router = new Router();
		if ($controller = $router->get_controller()) {
			require $controller;

			$class = basename($controller, '.php') . '_Controller';
			if (class_exists($class) && is_subclass_of($class, 'Core')) {
				$parameters = $router->get_parameters();
				if (method_exists($class, $parameters[0])) {
					return $this->dispatch($class, array_shift($parameters), $parameters);
				} elseif (method_exists($class, 'index')) {
					return $this->dispatch($class, 'index', $parameters);
				}
			}

			return show_500();
		} elseif ($controller === FALSE) {
			return show_404();
		}
	}

	private function dispatch($class, $method, $parameters) {
		$class = new $class;

		if ( ! empty($parameters)) {
			return call_user_func_array(array($class, $method), $parameters);
		}

		return call_user_func(array($class, $method));
	}

	private function load_core() {
		require BASE_PATH . '/core/error.php';
		require BASE_PATH . '/core/router.php';
		require BASE_PATH . '/core/config.php';
		require BASE_PATH . '/core/loader.php';
		require BASE_PATH . '/core/cache.php';
		require BASE_PATH . '/core/core.php';
		require BASE_PATH . '/core/controller.php';
		require BASE_PATH . '/core/database.php';
		require BASE_PATH . '/core/model.php';
	}
}

# End of file