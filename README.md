### Requirements:
* AcceptPathInfo on
* mod_rewrite
* PHP 5.4.17

### Features:
* Requires only 15 files (including 1 model, 1 view and 1 controller)
* PDO Database Connection
* Full page caching