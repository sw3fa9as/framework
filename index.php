<?php
define('BASE_PATH', __DIR__);
define('APPLICATION_PATH', __DIR__);
define('BASE_URL', '');

require BASE_PATH . '/core/application.php';
$application = new Application();
$application->run();
?>